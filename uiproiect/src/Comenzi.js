import React            from "react";
import InputField       from "./InputField";
import SubmitButton     from "./SubmitButton";
import axiosInstance    from 'axios';

class Comenzi extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            idProdus : 0,
            cantitate : 0
        }
    }

    setInputvalue(property, val){
        val = val.trim();
        if (val.length > 50)
        {
            return;
        }

        this.setState({
            [property] : val
        })
    }



    async doComanda(){
        if(!this.state.idProdus){
            return;
        }
        if(!this.state.cantitate){
            return;
        }

        axiosInstance
        .get("http://localhost:8080/getCurrentUser", )
        .then(response =>  {
            console.log(response.data);
            axiosInstance
            .post("http://localhost:8080/putComenzi",{
                id : null,
                idUser : response.data.id,
                idProdus : this.state.idProdus,
                cantitate : this.state.cantitate
            })
            .then(response =>  {
                console.log(response.data);
              })
              .catch(function (error) {
                console.log(error);
              });
          })
          .catch(function (error) {
            console.log(error);
          });
    }



    render(){
        
        return(
            <div className = "block_container">
                <div className = "loginForm">
                    Comanda
                    <InputField
                        type = "text"
                        placeholder = "Id produs"
                        value = {this.state.idProdus ? this.state.idProdus : ""}
                        onChange =   {(val) => this.setInputvalue("idProdus", val)}
                    />
                    <InputField
                        type = "text"
                        placeholder = "Cantitate"
                        value = {this.state.cantitate ? this.state.cantitate : ""}
                        onChange =   {(val) => this.setInputvalue("cantitate", val)}
                    />
                    <SubmitButton
                        text = 'Comanda'
                        disable = {false}
                        onClick = { () => this.doComanda() }
                    />
                </div>

            </div>
        );
    }


}

export default Comenzi;