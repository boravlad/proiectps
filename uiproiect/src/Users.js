import React from "react";
import {DataGrid} from '@material-ui/data-grid';
import axiosInstance from 'axios';
import {Button, Container, TextField} from  '@material-ui/core';

const columns = [
    {field: "id", headerName: "food id", width: 100 },
    {field: "price", headerName: "price", width: 100 },
    {field: "foodName", headerName: "Name", width: 100 }
]
export default class Users extends React.Component{
    constructor (){
        super();
        this.state={
            foods : [],
            id : 0
        };
        this.onSubmitDelete = this.onSubmitDelete.bind(this);
        axiosInstance
        .get("http://localhost:8080/getUser")
        .then(response => {
            const val = response.data
            //this.setState({foods: val})
            console.log("mancare")
            console.log(val)
        })
        .catch(error =>{
            console.log(error);
        })
    }
    handleInput = event => {

        const { value,name } = event.target;
        this.setState(
            {
                [name]: value
            }
        );
        console.log(value);
    };

    onSubmitDelete(){

        let delRes = this.state.id
    
        console.log( delRes)
    
        axiosInstance.delete("/admin/foodItem/" + delRes)
        .then(console.log("ok del"))
        .catch(error =>{
            console.log(error)
        })
    
    }

    render (){
        return (
            <div>
                <TextField
                            style={{'margin-right':'70px'}}
                            id="standard-basic"
                            label="Id"
                            onChange = {this.handleInput}
                            name="id" />

                <div style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
                    <div style={{ display: 'flex', height: 600,width:1750 }}>
                        <div style={{ flexGrow: 5 }}>
                            <DataGrid rows={this.state.foods} columns={columns} pageSize={20}/>
                        </div>

                    </div>

                </div>

                <div style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
                
                <div style = {{'margin-right':'20px'}}>
                        <Button
                                onClick={this.onSubmitF}
                                type = "submit"
                                variant="contained"
                                color="primary" >
                            Add
                        </Button>
                    </div>

                    <div style = {{'margin-right':'20px'}}>
                        <Button
                                onClick={this.onSubmitEdit}
                                type = "submit"
                                variant="contained"
                                color="inherit" >
                            Edit
                        </Button>
                    </div>

                    <div style = {{'margin-right':'20px'}}>
                        <Button
                                onClick={this.onSubmitDelete}
                                type = "submit"
                                variant="contained"
                                color="secondary" >
                            Delete
                        </Button>
                    </div>
                    </div>
                
            </div>
        );
    }
}

