import React            from "react";
import SubmitButton     from "./SubmitButton";
import axiosInstance    from 'axios';

class Raport extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            deAfisat : ''
        }
    }

    async doRaportUser(){
        axiosInstance
        .get("http://localhost:8080/getCurrentUser", )
        .then(response =>  {
            axiosInstance
            .post("http://localhost:8080/makeRaportUser",{
                id : response.data.id,
                nume : response.data.nume,
                prenume : response.data.prenume,
                email : response.data.email,
                parola : response.data.parola
            })
            .then(response =>  {
                const val = response.data
                this.setState({deAfisat: val})
              })
              .catch(function (error) {
                console.log(error);
              });
          })
          .catch(function (error) {
            console.log(error);
          });
    }


    async doRaportItems(){


        axiosInstance
        .get("http://localhost:8080/raportItems", )
        .then(response =>  {
            const val = response.data
            this.setState({deAfisat: val})
          })
          .catch(function (error) {
            console.log(error);
          });
    }



    render(){
        
        return(
            <div className = "block_container">
                <div className = "loginForm">
                    Afisarea rapoarte
                    <SubmitButton
                        text = 'Raporte total obiecte'
                        disable = {false}
                        onClick = { (val) => this.doRaportItems("deAfisat", val) }
                    />                    
                    <SubmitButton
                    text = 'Raport user'
                    disable = {false}
                    onClick = { (val) => this.doRaportUser("deAfisat", val) }
                    />
                </div>
                Raport-ul : {this.state.deAfisat}
            </div>
        );
    }


}

export default Raport;