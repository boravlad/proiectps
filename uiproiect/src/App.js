import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from "react-router-dom";

import LogIn from './LogIn';
import StoreItems from './StoreItems';
import Comenzi from './Comenzi';
import Raport from './Raport';

function App() {

  return (
    <Router>
      <div>
        <NavLink to = "/LogIn"> Logare </NavLink>
        <br></br>
        <NavLink to = "/StoreItems"> Obiecte </NavLink>
        <br></br>
        <NavLink to = "/Comenzi"> Comenzi </NavLink>
        <br></br>
        <NavLink to = "/Raport"> Raport </NavLink>
      <Switch>
      <Route exact path="/LogIn" component={LogIn}/>
      <Route exact path="/StoreItems" component={StoreItems}/>
      <Route exact path="/Comenzi" component={Comenzi}/>
      <Route exact path="/Raport" component={Raport}/>
      </Switch>
      </div>
  </Router>
  );
}

export default App;
