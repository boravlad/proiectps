import React           from "react";
import InputField       from "./InputField";
import SubmitButton     from "./SubmitButton";
import axiosInstance    from 'axios';
import {DataGrid} from '@material-ui/data-grid';


const columns = [
    {field: "id", headerName: "Id Obiect", width: 100 },
    {field: "nume", headerName: "Nume", width: 100 },
    {field: "cantitate", headerName: "Cantitate", width: 100 },
    {field: "descriere", headerName: "Descriere", width: 100 }
]

    
class StoreItems extends React.Component{;
    constructor(props){
        super(props);
        this.state = {
            nume: '',
            cantitate : 0,
            descriere : '',
            objects : []
        }


    }

    setInputvalue(property, val){
        if (val.length > 50)
        {
            return;
        }

        this.setState({
            [property] : val
        })
    }

    async doNewItem(){
        if(!this.state.nume){
            return;
        }
        if(!this.state.cantitate){
            return;
        }
        if(!this.state.descriere){
            return;
        }

        axiosInstance
        .post("http://localhost:8080/putItem", {
            id : null,
            nume : this.state.nume,
            cantitate : this.state.cantitate,
            descriere : this.state.descriere
        })
        .then(function (response) {
            console.log(response)
          })
          .catch(function (error) {
            console.log(error);
          });
    }

   async showAllItems(){

    axiosInstance
    .get("http://localhost:8080/getAllStoreItems")
    .then(response =>  {
        const val = response.data
        this.setState({objects: val})
      })
      .catch(function (error) {
        console.log(error);
      });
    }


    render(){
        return(
            <div className = "block_container">
                <div className = "loginForm">
                    New Store Item
                    <InputField
                        type = "text"
                        placeholder = "Nume"
                        value = {this.state.nume ? this.state.nume : ""}
                        onChange =   {(val) => this.setInputvalue("nume", val)}
                    />
                    <InputField
                        type = "text"
                        placeholder = "Cantitate"
                        value = {this.state.cantitate ? this.state.cantitate : ""}
                        onChange =   {(val) => this.setInputvalue("cantitate", val)}
                    />
                    <InputField
                        type = "text"
                        placeholder = "Descriere"
                        value = {this.state.descriere ? this.state.descriere : ""}
                        onChange =   {() => this.setInputvalue()}
                    />
                    <SubmitButton
                        text = 'Salveaza obiect'
                        disable = {false}
                        onClick = { () => this.doNewItem() }
                    />
                    <SubmitButton
                        text = 'Arata toate obiectele'
                        disable = {false}
                        onClick = { (val) => this.showAllItems("objects", val) }
                    />
                </div>
                {console.log(this.state.objects)}
                <div style={{ display: 'flex', height: 600,width:1750 }}>
                        <div style={{ flexGrow: 5 }}>
                            <DataGrid rows={this.state.objects} columns={columns} pageSize={20}/>
                        </div>

                </div>

            </div>
        );
    }
}


export default StoreItems;