import React            from "react";
import InputField       from "./InputField";
import SubmitButton     from "./SubmitButton";
import axiosInstance    from 'axios';

class LogIn extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            registerNume : '',
            registerPrenume : '',
            registerEmail : '',
            registerParola : '',
            nextPage : false,
            buttonDisable: false
        }
    }

    setInputvalue(property, val){
        val = val.trim();
        if (val.length > 40)
        {
            return;
        }

        this.setState({
            [property] : val
        })
    }

    resetLogIn(){
        this.setState({
            email: '',
            password: '',
            registerNume : '',
            registerPrenume : '',
            registerEmail : '',
            registerParola : '',
            nextPage : false,
            buttonDisable: false
        })
    }

    async doLogin(){
        if(!this.state.email){
            return;
        }
        if(!this.state.password){
            return;
        }

        this.setState({
            buttonDisable: true
        })
        axiosInstance
        .post("http://localhost:8080/checkUser", {
            id : null,
            nume : null,
            prenume : null,
            email : this.state.email,
            parola : this.state.password
        })
        .then(response =>  {
            console.log(response.data);
            axiosInstance
            .post("http://localhost:8080/addCurrentUser",{
                id : response.data.id,
                nume : response.data.nume,
                prenume : response.data.prenume,
                email : response.data.email,
                parola : response.data.parola
            })
            .then(response =>  {
                console.log(response.data);
              })
              .catch(function (error) {
                console.log(error);
              });
          })
          .catch(function (error) {
            console.log(error);
          });
    }

    async doRegister(){
        this.props.ceva = 1;
        if(!this.state.registerEmail){
            return;
        }
        if(!this.state.registerNume){
            return;
        }
        if(!this.state.registerPrenume){
            return;
        }
        if(!this.state.registerParola){
            return;
        }

        this.setState({
            buttonDisable: true
        })
        axiosInstance
        .post("http://localhost:8080/putUser", {
            id : null,
            nume : this.state.registerNume,
            prenume : this.state.registerPrenume,
            email : this.state.registerEmail,
            parola : this.state.registerParola
        })
        .then(response =>  {
            console.log(response.data);
            axiosInstance
            .post("http://localhost:8080/addCurrentUser", {
                id : response.data.id,
                nume : response.data.nume,
                prenume : response.data.prenume,
                email : response.data.email,
                parola : response.data.parola
            })
            .then(response =>  {
                console.log(response);
              })
              .catch(function (error) {
                console.log(error);
              });
          })
          .catch(function (error) {
            console.log(error);
          });
    }

    render(){
        
        return(
            <div className = "block_container">
                <div className = "loginForm">
                    Log in
                    <InputField
                        type = "text"
                        placeholder = "Email"
                        value = {this.state.email ? this.state.email : ""}
                        onChange =   {(val) => this.setInputvalue("email", val)}
                    />
                    <InputField
                        type = "password"
                        placeholder = "Password"
                        value = {this.state.password ? this.state.password : ""}
                        onChange =   {(val) => this.setInputvalue("password", val)}
                    />
                    <SubmitButton
                        text = 'Login'
                        disable = {this.state.buttonDisable}
                        onClick = { () => this.doLogin() }
                    />
                </div>
                <div className = "loginForm">
                    Register 
                    <InputField
                        type = "text"
                        placeholder = "Nume"
                        value = {this.state.registerNume ? this.state.registerNume : ""}
                        onChange =   {(val) => this.setInputvalue("registerNume", val)}
                    />
                    <InputField
                        type = "text"
                        placeholder = "Prenume"
                        value = {this.state.registerPrenume ? this.state.registerPrenume : ""}
                        onChange =   {(val) => this.setInputvalue("registerPrenume", val)}
                    />
                    <InputField
                        type = "text"
                        placeholder = "Email"
                        value = {this.state.registerEmail ? this.state.registerEmail : ""}
                        onChange =   {(val) => this.setInputvalue("registerEmail", val)}
                    />
                    <InputField
                        type = "password"
                        placeholder = "Parola"
                        value = {this.state.registerParola ? this.state.registerParola : ""}
                        onChange =   {(val) => this.setInputvalue("registerParola", val)}
                    />
                    <SubmitButton
                        text = 'Register'
                        disable = {this.state.buttonDisable}
                        onClick = { () => this.doRegister() }
                    />
                </div>

            </div>
        );
    }


}

export default LogIn;