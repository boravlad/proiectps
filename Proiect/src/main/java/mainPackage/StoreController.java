package mainPackage;


import mainPackage.dao.StoreItemRepository;
import mainPackage.model.StoreItem;
import mainPackage.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class StoreController {

    @Autowired
    private StoreItemRepository storeItemRepository;

    @GetMapping("/getStoreItemNew")
    public StoreItem getNewStoreItem()
    {
        return new StoreItem();
    }

    @GetMapping("/getStoreList")
    public List<StoreItem> getNewStoreLIst()
    {
        return new ArrayList<StoreItem>();
    }

    @GetMapping("/getAllStoreItems")
    public List<StoreItem> getAllStoreItems()
    {
        return storeItemRepository.findAll();
    }

    @PostMapping(path="/putItem", consumes="application/json", produces = "application/json")
    public StoreItem putItem(@RequestBody StoreItem storeItem)
    {
        return  storeItemRepository.save(storeItem);
    }
}
