package mainPackage;

import mainPackage.dao.UserRepository;
import mainPackage.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
public class AppController {
    @Autowired
    private UserRepository userRepo;


    @GetMapping("/getUser")
    public User getUser()
    {
        return new User();
    }

    @PostMapping(path="/checkUser", consumes="application/json", produces = "application/json")
    public String findUser(@RequestBody User newUser)
    {
        User user = userRepo.findByEmail(newUser.getEmail(), newUser.getParola());
        if (user == null)
        {
            return "checkUser";
        }
        return "paginaPrincipala";
    }

    @PostMapping(path="/putUser", consumes="application/json", produces = "application/json")
    public String putUser(@RequestBody User newUser)
    {
        userRepo.save(newUser);
        return "paginaPrincipala";
    }

}
