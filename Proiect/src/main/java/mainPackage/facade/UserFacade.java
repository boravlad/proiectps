package mainPackage.facade;

import mainPackage.dao.UserRepository;
import mainPackage.model.Comenzi;
import mainPackage.model.StoreItem;
import mainPackage.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;


import java.util.List;

/**
 * Aceasta clasa este un wrapper care face legatura intre logica aplicatiei si baza de date.
 */
@Service
public class UserFacade {

    @Autowired
    private UserRepository repository;

    /**
     * Aceasta este un constructor gol
     */
    public UserFacade() {

    }

    /**
     * Aceasta este un contructor pentru repository
     * @param repository aceasta este noul repository
     */
    public UserFacade(UserRepository  repository) {
        this.repository = repository;
    }

    /**
     * Aceeasta metoda are rostul de a salva un user in baza de date
     * @param u Acesta este userul salvazt
     */
    public User save(User u) {
        return this.repository.save(u);
    }


    /**
     * Aceasta metoda are scopul de a cauta un user dupa email
     * @param email acest parametrul este mail-ul dupa care se cauta user-ul
     * @return se returneaza user-ul cu mail-ul corespunzator
     */
    public User findByEmail(String email) {
        return this.repository.findByEmail(email);
    }

    /**
     * Aceasta metoda are scopul de a cauta un user dupa email si parola
     * @param email acest parametrul este mail-ul dupa care se cauta user-ul
     * @param pas cest parametrul este parola dupa care se cauta user-ul
     * @return acesta este user-ul cu parola si mail-ul corespunzator
     */
    public User findByEmailAndPass(String email, String pas) {
        return this.repository.findByEmailAndPass(email, pas);
    }

    /**
     * Aceasta metoda returneaza toate randurile din tabela mea de utilizatori.
     * @return Aceasta va fi lista rezultata prin colectia tuturor randurilor.
     */
    public List<User> findAll(){
        return repository.findAll();
    }
}
