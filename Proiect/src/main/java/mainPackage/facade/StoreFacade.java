package mainPackage.facade;

import mainPackage.dao.StoreItemRepository;
import mainPackage.dao.UserRepository;
import mainPackage.model.StoreItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Aceasta clasa este un wrapper care face legatura intre logica si baza de date.
 */
@Service
public class StoreFacade {

    @Autowired
    private StoreItemRepository repository;

    /**
     * Aceasta functie are ca scop de wrapper pentru metoda de save din repository
     * @param storeItem acesta este item-ul salvat
     * @return se va returna storeItem-ul salvat
     */
    public StoreItem save(StoreItem storeItem){
        return repository.save(storeItem);
    }

    /**
     * Aceasta metoda are rostul de a gasi toate storeItem-urile din baza de date
     * @return se va returna lista gasita
     */
    public List<StoreItem> findAll(){
        return repository.findAll();
    }

    /**
     * Aceasta metoda este un wrapper pentru functia de update si schimba continutul unui field deja prezent
     * @param storeItem acesta este item-ul care trebuie schimbat in baza de date
     */
    public void update(StoreItem storeItem){
        repository.findById(storeItem.getId())
                .map(storeItem1 -> {
                    storeItem1.setCantitate(storeItem.getCantitate());
                    return  repository.save(storeItem1);})
                .orElseGet(() -> {
                    return repository.save(storeItem);
                });
    }
}
