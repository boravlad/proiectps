package mainPackage.facade;

import mainPackage.dao.ComenziRepository;
import mainPackage.model.Comenzi;
import mainPackage.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Aceasta clasa este un wrapper intre intre repository-ul meu si partea de logica.
 */
@Service
public class ComenziFacade {

    @Autowired
    private ComenziRepository repository;

    /**
     * Aceasta metoda are rostul de a salva un obiect de tipul comanda in baza de date.
     * @param comenzi Acest parametru reprezinta comanda pe care vreau sa o salvez.
     * @return Se va returna comanda salvata.
     */
    public Comenzi save(Comenzi comenzi){
        return repository.save(comenzi);
    }

    /**
     * Aceast metoda are rostul de a returna toate randurile din baza mea de date.
     * @return Acest parametru va contine lista cu toate randurile tabelei.
     */
    public List<Comenzi> findAll(){
        return repository.findAll();
    }

    /**
     * Aceasta metoda are scopul de a returna toate comenzile unui anumit utilizator.
     * @param user Acesta este utilizatorul pentru care returnez comenzile.
     * @return Se va returna o lista cu toate randurile din baza de date ale unui utilizator.
     */
    public  List<Comenzi> findIdUser(User user){
        return repository.findByIdUser(user.getId());
    }
}
