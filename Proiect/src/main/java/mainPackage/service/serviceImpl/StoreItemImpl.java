package mainPackage.service.serviceImpl;

import mainPackage.dao.StoreItemRepository;
import mainPackage.facade.StoreFacade;
import mainPackage.model.Store;
import mainPackage.model.StoreItem;
import mainPackage.service.Observable;
import mainPackage.service.StoreItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Aceasta este o clasa in care vom avea o logica de verificare a parametrilor dati la nivelul aplicatiei.
 */
@Service
public class StoreItemImpl extends Observable implements StoreItemService {

    @Autowired
    private StoreFacade facade;

    @Autowired
    private StoreServiceImpl storeService;

    public void setFacade(StoreFacade facade) {
        this.facade = facade;
    }

    public void setStoreService(StoreServiceImpl storeService) {
        this.changeObserver(this.storeService, storeService);
        this.storeService = storeService;
    }

    /**
     * Aceasta metoda este folosita pentru a initializa un observer
     */
    @PostConstruct
    public void initialize() {
        this.addObserver(storeService);
    }


    private boolean checkDescriere(String descriere)
    {
        return descriere.length() < 100;
    }

    private boolean checkNume(String nume)
    {
        return nume.length() < 40;
    }

    /**
     * Aceasta metoda primeste un item si verifica daca este valid din punctul de vedere al datelor pentru a fi adaugat in baza de date.
     * Verificam daca lungimea numelui si lungimea descrierilor este valida.
     * @param storeItem Acesta este variabila verificata.
     * @return Vom returna daca verificarile au succes.
     */
    @Override
    public boolean checkData(StoreItem storeItem) {
        if (!checkDescriere(storeItem.getDescriere()))
            return false;
        if (!checkNume(storeItem.getNume()))
            return false;
        return true;
    }

    /**
     * Aceasta metoda este folosita pentru a returna toate itemele de vanzare din magazin
     * @return Se returneaza lista tuturor itemelor
     */
    public List<StoreItem> findAll(){
        return this.facade.findAll();
    }

    /**
     * Aceasta functie este folosita pentru a face update la baza de date
     * @param storeItem Acesta este storeItem-ul pe care il updatam
     */
    public void updateItem(StoreItem storeItem){
        facade.update(storeItem);
    }

    /**
     * Aceasta metoda este folosita pentru a adauga un item nou in baza noastra de date
     * @param storeItem Aceasta metoda este folosita pentru a adauga un nou item si pentru a realiza notify la observeri
     * @return Se returneaza noul item adaugat sau null in caz de esec
     */
    public StoreItem putItem(StoreItem storeItem){
        if (checkData(storeItem))
        {
            StoreItem myStoreItem = facade.save(storeItem);
            this.notifyObserver();
            return myStoreItem;
        }
        return null;
    }

    /**
     * Aceasta metoda este folosita pentru a creaza o noua lista de storeItems
     * @return aceasta este noua lista returnata
     */
    public List<StoreItem> makeStoreList(){
        return new ArrayList<StoreItem>();
    }

    /**
     * Aceasta metoda este folosita pentru a crea un nou storeItem
     * @return Acesta este noul storeItem returnat
     */
    public StoreItem makeStoreItem(){
        return new StoreItem();
    }
}
