package mainPackage.service.serviceImpl;

import mainPackage.Factory.RaportFacotry;
import mainPackage.Factory.RaportType;
import mainPackage.facade.ComenziFacade;
import mainPackage.facade.StoreFacade;
import mainPackage.facade.UserFacade;
import mainPackage.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Aceasta clasa contine logica pentru crearea rapoarteleor.
 */
@Service
public class RaportImpl {

    @Autowired
    private StoreFacade storeFacade;

    @Autowired
    private UserFacade userFacade;

    @Autowired
    private ComenziFacade comenziFacade;

    public RaportImpl(){

    }

    public RaportImpl(StoreFacade storeFacade, UserFacade userFacade, ComenziFacade comenziFacade){
        this.storeFacade = storeFacade;
        this.userFacade = userFacade;
        this.comenziFacade = comenziFacade;
    }

    /**
     * Aceasta metoda este folosita pentru a crea un nou raport customizat pentru utilizator.
     * @param user Aceasta parametru este utilizatorul pentru care vom crea un raport custom.
     * @return Aici se va returna raport-ul sub forma unui String.
     */
    public String getUserRaport(User user){
        List<Comenzi> userComenzi = comenziFacade.findIdUser(user);
        List<User> users = new ArrayList<User>();
        users.add(user);
        List<StoreItem> storeItems= storeFacade.findAll();
        RaportFacotry raportFacotry = new RaportFacotry();
        return raportFacotry.createCustomRaport(users, storeItems, userComenzi, RaportType.USER).getRaport();
    }

    /**
     * Aceasta metoda este folosita pentru a crea un raport care contine statisticile tuturor produselor.
     * @return Acesta este raportul returnat sub formtat String.
     */
    public String getItemRaport(){
        List<Comenzi> userComenzi = comenziFacade.findAll();
        List<User> users = null;
        List<StoreItem> storeItems= storeFacade.findAll();
        RaportFacotry raportFacotry = new RaportFacotry();

        return raportFacotry.createCustomRaport(users, storeItems, userComenzi, RaportType.ITEM).getRaport();
    }
}
