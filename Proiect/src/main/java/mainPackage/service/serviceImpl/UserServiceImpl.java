package mainPackage.service.serviceImpl;

import mainPackage.facade.UserFacade;
import mainPackage.model.User;
import mainPackage.service.Observer;
import mainPackage.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Aceasta clasa are logica de verificare de parametrii pentru logica de inserare in baza de date.
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserFacade userFacade;

    public UserServiceImpl() {

    }

    public UserServiceImpl(UserFacade userFacade) {
        this.userFacade = userFacade;
    }
    /**
     * Aceasta functie verifica daca lungimea unor string-uri este conforma cu restrangerile aplicatiei.
     * @param params Acest parametru este string-ul pe care il vom verifica.
     * @return Vom returna rezultatul comparari.
     */
    public boolean checkLenght(String params) {
        final int length = 45;
        return params.length() <= length;
    }

    /**
     * Acesta metoda verifica daca mail-ul este unic.
     * @param email Acest string corespunde cu adresa de mail.
     * @return se returneaza daca adresa de mail a mai fost introdusa in aplicatie.
     */
    public boolean checkEmail(String email){
        User user = userFacade.findByEmail(email);
        return user == null;
    }

    /**
     * Acesta metoda verifica validitatea tuturor parametrilor unui user.
     * @param user Acesta este parametrul verificat.
     * @return se returneaza rezultatul verificarii.
     */
    @Override
    public boolean verifyUserData(User user) {
        if (!(checkLenght(user.getNume()) && checkLenght(user.getPrenume())))
            return  false;
        return checkEmail(user.getEmail());
    }

    public User getNewUser() {
        return new User();
    }

    /**
     * Aceasta functie are scopul de a cauta un user in baza de date
     * @param newUser acesta este userul pe care il cautam
     * @return in funcite de rezultatul cautarii decidem pe ce pagina mergem
     */
    public User findUser(User newUser) {
        User user = userFacade.findByEmailAndPass(newUser.getEmail(), newUser.getParola());
        return user;
    }

    /**
     * Aceasta metoda este folosita de a updata un nou user
     * @param newUser acesta este noul user pe care il punem
     * @return Aceasta este pagina pe care o vom returna
     */
    public User putUser(User newUser){
        if (!this.verifyUserData(newUser))
            return null;
        return this.userFacade.save(newUser);

    }


}
