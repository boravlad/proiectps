package mainPackage.service.serviceImpl;

import mainPackage.dao.ComenziRepository;
import mainPackage.facade.ComenziFacade;
import mainPackage.model.Comenzi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Aceasta clasa este servici-ul pentru logica de comenzi.
 */
@Service
public class ComenziImpl {

    @Autowired
    private ComenziFacade comenziFacade;

    /**
     * Aceasta metoda este folosita pentru a crea o noua comanda pentru a fi trimisa la frontend.
     * @return Aici se returneaza o noua instanta a clasei Comenzi.
     */
    public Comenzi getNewComanda(){
        return new Comenzi();
    }

    /**
     * Aceasta metoda este folosita pentru a salva o noua comanda in baza de date.
     * @param comenzi Aceasta este comanda care este trimisa pentru a fi salvata.
     * @return Aceasta este tot comanda salvata.
     */
    public String saveComenzi(Comenzi comenzi){
        comenziFacade.save(comenzi);
        return "comandaProcesata";
    }

}
