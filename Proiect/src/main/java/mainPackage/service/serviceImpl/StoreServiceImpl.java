package mainPackage.service.serviceImpl;

import mainPackage.facade.StoreFacade;
import mainPackage.model.Store;
import mainPackage.model.StoreItem;
import mainPackage.model.User;
import mainPackage.service.Observer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Aceasta este o clasa in care vom avea o logica de verificare a parametrilor dati la nivelul aplicatiei.
 */
@Service
public class StoreServiceImpl implements Observer {

    @Autowired
    private StoreFacade storeFacade;

    private final Store store = new Store();

    /**
     * Aceasta metoda are scopul de a initializa user-ul in cadrul sesiunii curente
     * @param user acesta este user-ul logat la momentul curent
     */
    public void addCurrentUser(User user) {
        store.setUser(user);
    }

    public User getCurrentUser(){
        return store.getUser();
    }

    /**
     * Aceasta metoda are scopul de a initializa lista de produse disponibile din magazin
     * @param storeItemList
     */
    public void addTotalList(List<StoreItem> storeItemList){
        store.setStoreItemListAll(storeItemList);
    }

    /**
     * Aceasta functie adauga un storeItem la lista currenta
     * @param storeItem acesta este storeItem-ul adaugat
     */
    public void addItemToCurrent(StoreItem storeItem){
        store.getStoreCurrentItems().add(storeItem);
    }

    /**
     * Aceasta functie are rolul de a reface lista produselor disponibile in cazul in care unul nou a fost adaugat
     */
    @Override
    public void update() {
        store.setStoreItemListAll(storeFacade.findAll());
        System.out.println("PLACEHOLDER: Mesaj care spune sa s-a apelat functia update penrtu observer");
    }
}
