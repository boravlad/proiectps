package mainPackage.service;

import java.util.ArrayList;
import java.util.List;

/**
 * Aceasta este clasa de observable si o folosim pentru a defini clasele pe care le observam
 */
public abstract class Observable {

    List<Observer> observerList = new ArrayList<>();

    /**
     * Aceasta metoda este folosita pentru a adauga un observer la lista de observeri curenti
     * @param args Acest parametrul este folosit pentru a primi lista de observeri curenti
     */
    public void addObserver(Observer args){
        observerList.add(args);
    }

    /**
     * Aceasta metoda este folosita pentru a da notifi la toti observatorii curenti
     */
    public void notifyObserver(){
        for (Observer observer : observerList) {
            observer.update();
        }
    }

    /**
     * Aceasta metoda o folosim pentru a schimbi o instanta de observer cu alta intr-un anumit caz
     * @param toDelete acest parametru este parametrul pe care il inlocuim
     * @param toAdd acesta este parametrul nou
     */
    public void changeObserver(Observer toDelete, Observer toAdd) {
        observerList.remove(toDelete);
        observerList.add(toAdd);
    }
}
