package mainPackage.service;

/**
 * Aceasta interfata este folosita pentru a implementa logica de observer
 */
public interface Observer {

    public void update();
}
