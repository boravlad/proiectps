package mainPackage.service;

import mainPackage.model.StoreItem;

/**
 * Acesta interfata este implementata pentru o mai buna utilizare a codului si pentru a atinge un nivel de generalizare.
 */
public interface StoreItemService {
    /**
     * Aceasta functie are rolul de a verificare parametrii unui StoreItem.
     * @param storeItem Acesta este parametrul verificat.
     * @return Se va returna rezultatul verificarii.
     */
    public boolean checkData(StoreItem storeItem);
}
