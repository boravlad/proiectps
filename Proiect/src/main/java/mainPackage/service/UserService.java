package mainPackage.service;


import mainPackage.model.User;

/**
 * Acesta interfata este implementata pentru o mai buna utilizare a codului si pentru a atinge un nivel de generalizare.
 */
public interface UserService {

    /**
     * Aceasta metoda este folosita pentru a verifica toti parametrii unui user.
     * @param user Acesta este user-ul pe care il verificam.
     * @return Vom returna rezultatul verificarii.
     */
    public boolean verifyUserData(User user);

    /**
     * Aceasta metoda este folosita pentru a verifica lungimea String-urilor.
     * @param params Acesta este parametrul vericicat.
     * @return Vom returna rezultatul verificarii.
     */
    public boolean checkLenght(String params);

    /**
     * Aceasta metoda este folosita pentru a verifica daca un email este unic.
     * @param email Acesta este parametrul vericicat.
     * @return Vom returna rezultatul verificarii.
     */
    public boolean checkEmail(String email);
}
