package mainPackage.dao;

import mainPackage.model.Comenzi;
import mainPackage.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Aceasta interfata are rolul de a accesta tabelul de Comenzi din baza de date.
 */
public interface ComenziRepository extends JpaRepository<Comenzi, Long> {
    /**
     * Aceasta metoda este folosita pentru a gasi o comanda dupa id-ul unui user.
     * @param idUser Acesta este parametrul pe care il vom cauta.
     * @return Se va returna o lista a tuturor comenzilor unui utilizor.
     */
    @Query("SELECT c FROM Comenzi c WHERE c.idUser = ?1")
    public List<Comenzi> findByIdUser(Long idUser);

    /**
     *  Aceasta este o metoda care retuneaza toate comenzile care au un anumit produs.
     * @param idProdus Acesta este un parametru care denota idProdusului pe care il cautam.
     * @return Aici se returneaza o lista cu toate comenzile care au un anumit produs.
     */
    @Query("SELECT c FROM Comenzi c WHERE c.idProdus = ?1")
    public List<Comenzi> findByIdProdus(Long idProdus);
}
