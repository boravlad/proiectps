package mainPackage.dao;

import mainPackage.model.StoreItem;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Aceasta interfata are rolul de a accesta tabelul StoreItem din baza de date.
 */
public interface StoreItemRepository extends JpaRepository<StoreItem, Long> {
}
