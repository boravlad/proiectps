package mainPackage.dao;

import mainPackage.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

/**
 * Aceasta interfata are rolul de a accesta tabelul User din baza de date.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    /**
     * Aceasta metoda are rolul de a cauta un user dupa email si parola.
     * @param email Aceasta este email-ului user-ului pe care il cautam.
     * @param parola Aceasta este parola user-ului pe care il cautam.
     * @return Acesta este user-ul pe care il returnam sau null in cazul in care nu il gasim.
     */
    @Query("SELECT u FROM User u WHERE u.email = ?1 AND u.parola = ?2")
    public User findByEmailAndPass(String email, String parola);

    /**
     * Aceasta metoda este folosita pentru a cauta un user doar dupa email.
     * @param email Acesta este email-ul cautat de noi.
     * @return Acesta este user-ul pe care il gasim sau null in cazul in care nu il gasim
     */
    @Query("SELECT u FROM User u WHERE u.email = ?1")
    public User findByEmail(String email);


}
