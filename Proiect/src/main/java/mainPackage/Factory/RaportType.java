package mainPackage.Factory;

/**
 * Aceast enum este folosit pentru design pattern-ul factory pentru a diferentia intre tip-ul de rapoarte de care avem nevoie.
 */
public enum RaportType {
    USER (1),
    ITEM (2);
    private int type;
    RaportType(int type)
    {
        this.type = type;
    }

}
