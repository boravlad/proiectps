package mainPackage.Factory;

import mainPackage.Factory.RaportType;
import mainPackage.model.*;

import java.util.List;

/**
 * Aceasta este o clasa unde se implementeaza design pattern-ul factory pentru crearea abstracta a obiectelor.
 */
public class RaportFacotry {

    /**
     * Aceasta metoda se constituie implementare design pattern-ul si aici se creaza tipurile diferite.
     * @param users Aceasta este lista de utilizatori pe care o folosim pentru instantierea obiectelor.
     * @param items Aceasta este lista de iteme din magazin pe care o folosim pentru instantiere.
     * @param comenzi Aceasta este lista de comenzi pe care o folosim la instantiere.
     * @param raportType Aceasta este o variabila care ne spune ce tip de raport trebuie sa facem.
     * @return Se returneaza un raport generic.
     */
    public Raport createCustomRaport(List<User> users, List<StoreItem> items, List<Comenzi> comenzi, RaportType raportType) {
        Raport raport = null;
        RaportType type = raportType;
        if (type != null) {
            switch (type) {
                case USER:
                    raport = new RaportUser(users, items, comenzi, type);
                    break;
                case ITEM:
                    raport = new RaportItem(users, items, comenzi, type);
                    break;
                default:
                    System.err.println("Tip de raport nedefinit.");
            }
        } else {
            System.err.println("Tip de raport nedefinit.");
        }
        return raport;
    }
}

