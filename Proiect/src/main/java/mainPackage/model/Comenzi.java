package mainPackage.model;

import javax.persistence.*;

/**
 * Aceasta clasa are rolul de a fi o abstractizare a modelul de comanda din aplicatie, vom avea aici un idUser, un idProdus, si o cantiate,
 * fiecare comanda va avea un singur user si un singur produs. O comanda cu mai multe produse se va separa la nivel de aplicatie.
 */
@Entity
@Table
public class Comenzi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long idUser;

    @Column
    private Long idProdus;

    @Column
    private Long cantitate;

    public Comenzi(){

    }

    public Comenzi(Long idUser, Long idProdus) {
        this.idUser = idUser;
        this.idProdus = idProdus;
    }

    public Long getCantitate() {
        return cantitate;
    }

    public void setCantitate(Long cantitate) {
        this.cantitate = cantitate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdProdus() {
        return idProdus;
    }

    public void setIdProdus(Long idProdus) {
        this.idProdus = idProdus;
    }
}
