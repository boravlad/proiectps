package mainPackage.model;

import mainPackage.Factory.RaportType;

import java.util.List;

/**
 * Aceasta clasa este folosita pentru a ii prezenta utilizatorului raport-ul pe care acesta il cere.
 */
public abstract class Raport {

    private List<User> users;
    private List<StoreItem> storeItems;
    private List<Comenzi> comenziList;
    private RaportType raportType;

    public Raport(){

    }

    public Raport(List<User> users, List<StoreItem> storeItems, List<Comenzi> comenziList, RaportType raportType) {
        this.users = users;
        this.storeItems = storeItems;
        this.comenziList = comenziList;
        this.raportType = raportType;
    }

    public Raport(RaportType raportType){
        this.raportType = raportType;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<StoreItem> getStoreItems() {
        return storeItems;
    }

    public void setStoreItems(List<StoreItem> storeItems) {
        this.storeItems = storeItems;
    }

    public List<Comenzi> getComenziList() {
        return comenziList;
    }

    public void setComenziList(List<Comenzi> comenziList) {
        this.comenziList = comenziList;
    }

    public  abstract String getRaport();
}
