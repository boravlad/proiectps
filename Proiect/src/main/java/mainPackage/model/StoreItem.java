package mainPackage.model;


import javax.persistence.*;

/**
 * Clasa StoreItem are scopul de a fi o abstractizare a itemelor pe care Userul o sa le poate cumpara la nivelul aplicatiei.
 */
@Entity
@Table
public class StoreItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 45)
    private String nume;

    @Column
    private Long cantitate;

    @Column(length = 100)
    private String descriere;

    public StoreItem() {

    }

    public StoreItem(Long id, String nume, Long cantitate, String descriere) {
        this.id = id;
        this.nume = nume;
        this.cantitate = cantitate;
        this.descriere = descriere;
    }

    public StoreItem(String nume, Long cantitate, String descriere) {
        this.nume = nume;
        this.cantitate = cantitate;
        this.descriere = descriere;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public Long getCantitate() {
        return cantitate;
    }

    public void setCantitate(Long cantitate) {
        this.cantitate = cantitate;
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }
}
