package mainPackage.model;

import java.util.Arrays;
import java.util.List;

/**
 * Aceasta clasa are scopul de a face legatura intre toate obiectele noastre.
 */
public class Store {
    private User user;
    private List<StoreItem> storeItemListAll;
    private List<StoreItem> storeCurrentItems;

    public Store(){

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<StoreItem> getStoreItemListAll() {
        return storeItemListAll;
    }

    public void setStoreItemListAll(List<StoreItem> storeItemListAll) {
        this.storeItemListAll = storeItemListAll;
    }

    public List<StoreItem> getStoreCurrentItems() {
        return storeCurrentItems;
    }

    public void setStoreCurrentItems(List<StoreItem> storeCurrentItems) {
        this.storeCurrentItems = storeCurrentItems;
    }
}
