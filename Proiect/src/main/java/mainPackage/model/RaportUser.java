package mainPackage.model;

import mainPackage.Factory.RaportType;

import java.util.List;

/**
 * Aceasta clasa are scopul de a crea un raport customizat pentru un anumit utilizator.
 */
public class RaportUser extends Raport{


    public RaportUser() {
    }

    public RaportUser(List<User> users, List<StoreItem> storeItems, List<Comenzi> comenziList, RaportType raportType) {
        super(users, storeItems, comenziList, raportType);
    }

    public RaportUser(RaportType raportType) {
        super(raportType);
    }

    /**
     * Aceasta clasa este folosita pentru a crea un raport custom pentru utilizatorul nostru.
     * @return Acest parametru este raportul pe care noi il vom crea.
     */
    @Override
    public String getRaport() {
        String toReturn = "";
        for (User user : getUsers())
        {
            toReturn += user.getNume() + " " + user.getPrenume() + "\n";
            for (StoreItem current : this.getStoreItems())
            {
                int suma = 0;
                for(Comenzi currentComanda : this.getComenziList())
                {
                    if (currentComanda.getIdProdus().equals(current.getId()) && user.getId().equals(currentComanda.getIdUser()))
                    {
                        suma += currentComanda.getCantitate();
                    }
                }
                if (suma > 0)
                {
                    toReturn = toReturn + current.getNume() + ": ";
                    toReturn = toReturn + suma + "\n";
                }
            }
        }
        return toReturn;
    }
}
