package mainPackage.model;

import mainPackage.Factory.RaportType;

import java.util.List;

/**
 * Aceasta clasa reprezinta un raport custom pentru utilizatorul nostru care prezinta doar itemele.
 */
public class RaportItem extends Raport{

    public RaportItem() {
    }

    public RaportItem(List<User> users, List<StoreItem> storeItems, List<Comenzi> comenziList, RaportType raportType) {
        super(users, storeItems, comenziList, raportType);
    }

    public RaportItem(RaportType raportType) {
        super(raportType);
    }

    /**
     * Aceata metoda va crea un raport din listele noastre.
     * @return Aici se retuneaza raport-ul pe care il facem noi.
     */
    @Override
    public String getRaport() {
        String toReturn = "";
        for (StoreItem current : this.getStoreItems())
        {
            toReturn = toReturn + current.getNume() + ": ";
            int suma = 0;
            for(Comenzi currentComanda : this.getComenziList())
            {
                if (currentComanda.getIdProdus() == current.getId())
                {
                    suma += currentComanda.getCantitate();
                }
            }
            toReturn = toReturn + suma + "\n";
        }
        return toReturn;
    }
}
