package mainPackage.controllers;

import mainPackage.model.StoreItem;
import mainPackage.model.User;
import mainPackage.service.serviceImpl.StoreServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Aceasta clasa este folosita pentru a face legatura intre diferitele functionalitati ale aplicatiei.
 */
@RestController
public class MainStoreController {

    @Autowired
    private StoreServiceImpl storeService;

    /**
     * Aceasta metoda are scopul de a initializa utilizatorul conectat current
     * @param user Acesta este utilizatorul adaugat
     */
    @PostMapping("/addCurrentUser")
    public void addCurrentUser(@RequestBody User user)
    {
        storeService.addCurrentUser(user);
    }

    @GetMapping("/getCurrentUser")
    public User addCurrentUser()
    {
        return storeService.getCurrentUser();
    }

    /**
     * Aceasta lista adauga la lista curenta o lista de iteme
     * @param storeItemList acesta este lista adaugata
     */
    @PostMapping("/addStoreList")
    public void addStoreItemList(@RequestBody List<StoreItem> storeItemList)
    {
        storeService.addTotalList(storeItemList);
    }
}
