package mainPackage.controllers;

import mainPackage.model.User;
import mainPackage.service.serviceImpl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *  Aceasta clasa este folosita pentru a face legatura intre logica de User si logica de prezentare a proiectului.
 */
@RestController
public class UserController {


    @Autowired
    private UserServiceImpl userService;

    /**
     * Aceasta metoda este folosita pentru a returna un nou user care va urma sa fie instantiat.
     * @return Se returneaza noul urez creat.
     */
    @GetMapping("/getUser")
    public User getUser() {
        //return userService.getNewUser();
        return new User("ceva", "altceva", "ceva@mail", "parola");
    }

    /**
     *  Aceasta metoda este folosita pentru pentru a cauta un nou user deja creat in baza de date.
     * @param newUser Acest parametru reprezinta user-ul pe care il cautam in baza de date.
     * @return Parametrul returnat este o noua pagina in de rezultatul logari.
     */
    @PostMapping(path="/checkUser", consumes="application/json", produces = "application/json")
    public User findUser(@RequestBody User newUser) {
        return userService.findUser(newUser);
    }

    /**
     * Aceasta metoda este folosita pentru a verifica daca putem sa inseram un nou user dupa anumire criterii.
     * @param newUser Acesta este noul user pe care am dori sa il inseram.
     * @return Aceasta este un string care denota rezultatul operatiei noastre.
     */
    @PostMapping(path="/putUser", consumes="application/json", produces = "application/json")
    public User putUser(@RequestBody User newUser) {
        return userService.putUser(newUser);
    }

}
