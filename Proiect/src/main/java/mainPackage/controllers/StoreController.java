package mainPackage.controllers;


import mainPackage.dao.StoreItemRepository;
import mainPackage.model.StoreItem;
import mainPackage.service.serviceImpl.StoreItemImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Aceasta clasa este un controller care are scopul de a face legatura intre aplicatie si logica de legare pentru partea de storeItems.
 */
@RestController
public class StoreController {


    @Autowired
    private StoreItemImpl storeItemImpl;

    /**
     *  Aceasta metoda va returna un StoreItem pentru aplicatie ca sa poata fi modificat.
     * @return Se va returna noua variabila creata.
     */
    @GetMapping("/getStoreItemNew")
    public StoreItem getNewStoreItem() {
        return storeItemImpl.makeStoreItem();
    }

    /**
     * Aici se creeaza o lista care se va instantia mai tarziu la nivel de aplicatie.
     * @return Acest paremetru este lista nou creata.
     */
    @GetMapping("/getStoreList")
    public List<StoreItem> getNewStoreLIst() {
        return storeItemImpl.makeStoreList();
    }

    /**
     * Aceasta metoda returneaza toate itemele din magazin.
     * @return Aceasta este lista tuturor obiectelor de tip StoreItem din aplicatie.
     */
    @GetMapping("/getAllStoreItems")
    public List<StoreItem> getAllStoreItems() {
        return storeItemImpl.findAll();
    }

    /**
     * Aceasta metoda are scopul de a adauga in aplicatie un nou item.
     * @param storeItem Acesta este item-ul care il vom adauga in baza de date.
     * @return Il vom returna in caz de succes, iar in caz de esec vom returna null.
     */
    @PostMapping(path="/putItem", consumes="application/json", produces = "application/json")
    public StoreItem putItem(@RequestBody StoreItem storeItem)
    {
        return storeItemImpl.putItem(storeItem);
    }

    /**
     * Aceasta metoda este folosita pentru a face un update in cazul dorim sa scadem numarul unor produse
     * din stoc in urma unei comenzi.
     * @param storeItem Acest parametru este noul parametru pe care il vom schimba in baza de date.
     */
    @PutMapping(path="/updateItem", consumes="application/json", produces = "application/json")
    public void updateItem(@RequestBody StoreItem storeItem) {
        storeItemImpl.updateItem(storeItem);
    }
}
