package mainPackage.controllers;

import mainPackage.dao.ComenziRepository;
import mainPackage.model.Comenzi;
import mainPackage.service.serviceImpl.ComenziImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Aceasta clasa este un controller care are rolul de a lega clasele care genereaza comenzi ale unui utilizator.
 */
@RestController
public class ComenziController {

    @Autowired
    private ComenziImpl comenzi;

    /**
     * Aceasta metoda are scopul de a crea un obiect de tipul de date Comenzi si al da la interfata.
     * @return Se returneaza noua comanda formata.
     */
    @GetMapping("/getComenzi")
    public Comenzi getComenzi() {
        return comenzi.getNewComanda();
    }

    /**
     *  Aceasta metoda are scopul de a salva in baza de date comanda data de utilizator dupa confirmarea acesteia.
     * @param newComanda Acest parametru este comanda pe care o salvam.
     * @return Se returneaza pagina urmatoare la care am trece in contextul aplicatiei.
     */
    @PostMapping(path="/putComenzi", consumes="application/json", produces = "application/json")
    public String putComanda(@RequestBody Comenzi newComanda) {
        return  comenzi.saveComenzi(newComanda);
    }
}
