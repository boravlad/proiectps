package mainPackage.controllers;

import mainPackage.dao.ComenziRepository;
import mainPackage.model.StoreItem;
import mainPackage.model.User;
import mainPackage.service.serviceImpl.RaportImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Aceasta clasa este folosita pentru a face cereri de a crea rapoarte.
 */
@RestController
public class RaportController {

    @Autowired
    private RaportImpl raport;

    /**
     * Aceasta metoda este folosita pentru a trimite o cerere de creare a unui raport.
     * @param newUser Acest parametru este folosit pentru a primi utilizatorului al carui raport se contruieste.
     * @return String returnat va fi raport-ul creat pentru utilizatorul respectiv.
     */
    @PostMapping(path="/makeRaportUser", consumes="application/json", produces = "application/json")
    public String getRaportUser(@RequestBody User newUser) {
        return raport.getUserRaport(newUser);
    }


    /**
     * Aceasta metoda este folosita pentru a trimite o cerere de creare a unui raport pentru toate itemele din magazin.
     * @return String returnat va fi raport-ul creat pentru itemele magazinului.
     */
    @GetMapping(path="/raportItems")
    public String getRaportItems(){
        System.out.println(raport.getItemRaport());
        return raport.getItemRaport();
    }

}
