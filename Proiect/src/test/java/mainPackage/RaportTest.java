package mainPackage;

import mainPackage.facade.ComenziFacade;
import mainPackage.facade.StoreFacade;
import mainPackage.facade.UserFacade;
import mainPackage.service.serviceImpl.RaportImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class RaportTest {

    @Mock
    private StoreFacade storeFacade;

    @Mock
    private UserFacade userFacade;

    @Mock
    private ComenziFacade comenziFacade;


    @Test
    public void TestServiceRaport()
    {
        RaportImpl raportIml = new RaportImpl(storeFacade, userFacade, comenziFacade);
        raportIml.getItemRaport();
        verify(storeFacade, atLeastOnce()).findAll();
        verify(comenziFacade, atLeastOnce()).findAll();
    }
}
