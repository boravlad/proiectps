package mainPackage;

import mainPackage.dao.UserRepository;
import mainPackage.facade.UserFacade;
import mainPackage.model.User;
import mainPackage.service.serviceImpl.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class TestMockUser {

    @Mock
    UserRepository userRepository;


    @Test
    public void testVerification()
    {
        assertNotNull(userRepository);
        UserFacade userFacade = new UserFacade(userRepository);
        UserServiceImpl userService = new UserServiceImpl(userFacade);
        User user = new User("Bora", "Vlad", "123@gmail.com", "cevaParola");
        boolean check = userService.verifyUserData(user);
        assertTrue(check);
        verify(userRepository, atLeastOnce()).findByEmail(user.getEmail());
    }

    @Test
    public  void testEarlyTermination()
    {
        assertNotNull(userRepository);
        UserFacade userFacade = new UserFacade(userRepository);
        UserServiceImpl userService = new UserServiceImpl(userFacade);
        User user = new User("VladVladVladVladVladVladVladVladVladVladVladVladVladVladVladVladVladVladVladVladVladVlad", "Vlad", "123@gmail.com", "cevaParola");
        boolean check = userService.verifyUserData(user);
        Assertions.assertFalse(check);
        verify(userRepository, never()).findByEmail(user.getEmail());
    }

    @Test
    public void testInputs()
    {
        assertNotNull(userRepository);
        UserFacade userFacade = new UserFacade(userRepository);
        UserServiceImpl userService = new UserServiceImpl(userFacade);
        User user1 = new User("VladVladVladVladVladVladVladVladVladVladVladVladVladVladVladVladVladVladVladVladVladVlad", "Vlad", "123@gmail.com", "cevaParola");
        boolean check = userService.checkEmail(user1.getEmail());
        Assertions.assertTrue(check);
        check = userService.checkLenght(user1.getNume());
        Assertions.assertFalse(check);
        verify(userRepository, atLeastOnce()).findByEmail(user1.getEmail());
    }

}
