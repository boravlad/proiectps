package mainPackage;


import mainPackage.facade.StoreFacade;
import mainPackage.model.StoreItem;
import mainPackage.service.serviceImpl.StoreItemImpl;
import mainPackage.service.serviceImpl.StoreServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
public class TestMockStore {

    @Mock
    private StoreFacade storeFacade;

    @Mock
    private StoreServiceImpl storeService;

    @Test
    public void TestObserver()
    {
        assertNotNull(storeFacade);
        assertNotNull(storeService);
        StoreItemImpl storeItemService = new StoreItemImpl();
        storeItemService.setStoreService(storeService);
        storeItemService.setFacade(storeFacade);
        StoreItem myStoreItem = new StoreItem("cevaDenumire", 12L, "cevaDescriere");
        storeItemService.putItem(myStoreItem);
        verify(storeService, atLeastOnce()).update();
    }
}
