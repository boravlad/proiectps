package mainPackage;

import mainPackage.Factory.RaportFacotry;
import mainPackage.Factory.RaportType;
import mainPackage.model.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class FactoryTest {

    @Test
    public void testFactoryType() {
        RaportFacotry raportFacotry = new RaportFacotry();
        Raport raport =  raportFacotry.createCustomRaport(null, null, null, RaportType.USER);
        Assertions.assertTrue(raport instanceof RaportUser);
        raport =  raportFacotry.createCustomRaport(null, null, null, RaportType.ITEM);
        Assertions.assertTrue(raport instanceof RaportItem);
        Assertions.assertEquals("cevaString", raport.getRaport());
    }

    @Test
    public void testFactory(){
        RaportFacotry raportFacotry = new RaportFacotry();
        List<User> users = new ArrayList<User>();
        users.add(new User(1L, "cevaNume", "cevaPrenuma", "cevaMail", "cevaParola"));
        List<StoreItem> storeItems = new ArrayList<>();
        storeItems.add(new StoreItem(1L, "cevaNume", 100L, "cevaDescriere"));
        List<Comenzi> comenziList = new ArrayList<>();
        comenziList.add(new Comenzi(1L,1L));
        Raport raport =  raportFacotry.createCustomRaport(users, storeItems, comenziList, RaportType.USER);
        Assertions.assertEquals("cevaNume cevaPrenuma\ncevaNume: 1\n", raport.getRaport());
    }
}
