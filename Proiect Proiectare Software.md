# Proiect Proiectare Software
## _Online store_
Nume Prenume: Bora Vlad
Grupa: 30239

Aceasta aplicatie are scopul de a facilita un mediu usor in care utilizatorul poate sa isi comande diferite obiecte si de asemenea poate sa vanda diverse lucruri.

## End points

- Utilizatorul poate sa isi creeeze un cont si sa isi vada comenzile  
- Utilizatorul poate sa introduca diverse obiecte in scopul vanzarii
- Utilizatorul poate sa isi cumpere diverse obiecte
- La nivelul de aplicatie utilizatorul poate sa isi aleaga din toate produsele disponibile
- Atunci cand utilizatorul iti adauga un nou produs lista totale de produse se updateaza in timp real
- Utilizatorul poate sa ceara o serie de raporte personalizate sau pentru toate itemele din magazin

Urmeaza o prezentare generala a claselor pe care le-am implementat la nivelul aplicatiei, o prezentare a functionalitatilor actuale si o prezentare a functionalitatilor care urmeaza.


## Model
Clasele principale in pachetul model sunt Comenzi, StoreItem si User.
Aceste clase au rolul de a abstractiza conceptul lor real la nivelul de aplicatie.
Clasa store are rolul de a fi o clasa de legatura care sa uneasca datele din celelalte clase.
Clasele pentru crearea raportului.

## (DAO) Data Access Object
Acest pachet este compus dintr-o serie de interfete care au rolul de a accesa baza de date.
Fiecare clasa foloseste legaturi de implemntare a unor clase crud care folosesc quer-uri pentru a accesa baza de date.

## Controllers
In pachetul de controlere implementez diferite metode de Get, Post, Put cu scopul de a crea legaturi cu partea de frontend. Aceste metode au diferite utilitati, dar in general ele fac legatura intre logica aplicatiei, partea de baze de date si partea de frontend. Prin logica intelegem partea de verificare a datelor de intrare si generarea rezultatelor operatiilor.

## Service
In aceast pachet implementam clasele si interfetele folosite pentru logica proiectului de verificare a validitatii obiectelor, in sensul in care aceste pot sa fie reprezentate in baza noastra de date. In cazul in care parametrii nu sunt intre anumite constrangeri vom afisa o eroare. In partea de service avem si logica de legatura a claselor care este in clasa de store pe care o folosim la legatura obiectelor.

## Diagrame

![diagrama de deployment](https://i.ibb.co/ZdB8qTX/Deployment.png)
![diagrama uml](https://i.ibb.co/LvpJst9/UMLObserver.png)
![diagrama sequence](https://i.ibb.co/H217rPm/diagrama.png)

## Teste
La nivelul testelor s-a folosit mockito pentru a verifica flow-ul aplicatiei (daca anumite functii se apeleaza in anumite cazuri), de asemenea s-au implementat o serie de unit test-uri pentru a verifica daca metodele functioneaza si au un comportament asteptat.
La nivelul testelor pentru design pattern-ul de observer am testat flow-ul de la notify spre update-ul observer-ului.

## Dezvoltari ulterioare
In continuare vom face partea de front end, vom implementa o serie de design patter-uri pentru a observa schimbarile in aplicatie si vom implementa o serie de report-uri pentru a comunica utilizatorului mai bine istoricul.

